﻿using NUnit.Framework;
using static SharpBank.Account;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account((int)AccountTypes.CHECKING);
            Account savingsAccount = new Account((int)AccountTypes.SAVINGS);
            Customer henry = new Customer("Henry");
            henry.OpenAccount(checkingAccount);
            henry.OpenAccount(savingsAccount);

            checkingAccount.Deposit(100);
            savingsAccount.Deposit(4000);
            savingsAccount.Withdraw(200);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100,00\n" +
                    "Total $100,00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4.000,00\n" +
                    "  withdrawal $200,00\n" +
                    "Total $3.800,00\n" +
                    "\n" +
                    "Total In All Accounts $3.900,00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(new Account((int)AccountTypes.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(new Account((int)AccountTypes.SAVINGS));
            oscar.OpenAccount(new Account((int)AccountTypes.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(new Account((int)AccountTypes.SAVINGS));
            oscar.OpenAccount(new Account((int)AccountTypes.CHECKING));
            oscar.OpenAccount(new Account((int)AccountTypes.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTranseference()
        {
            Bank bank = new Bank();
            Customer oscar = new Customer("Oscar");
            Account savings = new Account((int)AccountTypes.SAVINGS);
            Account checking = new Account((int)AccountTypes.CHECKING);
            oscar.OpenAccount(savings);
            oscar.OpenAccount(checking);
            oscar.Trasference(savings, checking, 300);
            bank.AddCustomer(oscar);
            Assert.AreEqual(0, bank.TotalInterestPaid());
        }
    }
}

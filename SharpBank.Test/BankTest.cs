﻿using NUnit.Framework;
using static SharpBank.Account;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account((int)AccountTypes.CHECKING));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\r\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)AccountTypes.CHECKING);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)AccountTypes.SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountNoWithdrawals()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)AccountTypes.MAXI_SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(150, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithWithdrawals()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)AccountTypes.MAXI_SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Withdraw(3000);

            Assert.AreEqual(-300, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void GetFirstCustomer()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            bank.AddCustomer(bill);

            Assert.AreEqual(bill.Name, bank.GetFirstCustomer());
        }

        [Test]
        public void NoCustomers()
        {
            Bank bank = new Bank();

            Assert.AreEqual("No Customers", bank.GetFirstCustomer());
        }
    }
}

﻿namespace SharpBank
{
    class InterestSavings : Interest
    {
        public override double CalculateInterest(double amount, bool noWithdrawalsInTenDays)
        {
            if (amount <= 1000)
                return amount * 0.001;
            else
                return 1 + (amount - 1000) * 0.002;
        }
    }
}

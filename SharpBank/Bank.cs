﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public string CustomerSummary()
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("Customer Summary");
            foreach (var customer in customers)
            {
                summary.Append(" - " + customer.Name + " (" + Format(customer.GetNumberOfAccounts(), "account") + ")");
            }
            return summary.ToString();
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string Format(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (var customer in customers)
            {
                total += customer.TotalInterestEarned();
            }
            return total;
        }

        public string GetFirstCustomer()
        {
            try
            {
                return customers.FirstOrDefault().Name;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "No Customers";
            }
        }
    }
}

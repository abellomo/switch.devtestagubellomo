﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private List<Account> accounts;
        private List<Transfer> transferences;
        public string Name { get; }

        public Customer(string name)
        {
            Name = name;
            accounts = new List<Account>();
            transferences = new List<Transfer>();
        }

        public void OpenAccount(Account account)
        {
            accounts.Add(account);
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (var account in accounts)
            {
                total += account.InterestEarned();
            }

            return total;
        }

        public string GetStatement()
        {
            double total = 0;
            StringBuilder statement = new StringBuilder("Statement for " + Name + "\n\n");

            foreach (var account in accounts)
            {
                statement.Append(account.GetPrettyAccountType() + "\n");
                statement.Append(StatementForAccount(account));
                total += account.SumTransactions();
            }
            statement.Append("Total In All Accounts " + ToDollars(total));
            return statement.ToString();
        }

        private string StatementForAccount(Account account)
        {
            StringBuilder transactionByAccount = new StringBuilder();
            double total = 0;

            foreach (var transaction in account.transactions)
            {
                transactionByAccount.Append("  " + (transaction.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(transaction.amount) + "\n");
                total += transaction.amount;
            }
            transactionByAccount.Append("Total " + ToDollars(total) + "\n\n");

            return transactionByAccount.ToString();
        }

        private string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public void Trasference(Account fromAccount, Account toAccount, double amount)
        {
            if (accounts.Contains(fromAccount) && accounts.Contains(toAccount))
            {
                Transfer transfer = new Transfer(fromAccount, toAccount, amount);
                transferences.Add(transfer);
            }
        }
    }
}

﻿using System;

namespace SharpBank
{
    public class Transfer
    {
        public Account FromAccount { get; set; }
        public Account ToAccount { get; set; }
        public DateTime TransferenceDateTime { get; set; }
        public double Amount { get; set; }

        public Transfer(Account fromAccount, Account toAccount, double amount)
        {
            if (amount >= 0)
            {
                FromAccount = fromAccount;
                ToAccount = toAccount;
                Amount = amount;
                FromAccount.Withdraw(Amount);   
                ToAccount.Deposit(Amount);
                TransferenceDateTime = DateTime.Now;
            }
        }
    }
}

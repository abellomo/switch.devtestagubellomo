﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        public DateTime transactionDate { get; set; }

        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateTime.Now;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public abstract class Interest
    {
        public abstract double CalculateInterest(double amount, bool noWithdrawalsInTenDays);
    }
}

﻿namespace SharpBank
{
    class InterestCheckingAccounts : Interest
    {
        public override double CalculateInterest(double amount, bool noWithdrawalsInTenDays)
        {
            return amount * 0.001;
        }
    }
}

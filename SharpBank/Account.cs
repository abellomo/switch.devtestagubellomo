﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class Account
    {
        public enum AccountTypes
        {
            CHECKING = 0,
            SAVINGS = 1,
            MAXI_SAVINGS = 2
        }

        private Interest interest;
        private readonly int accountType;
        private bool noWithdrawalsInTenDays = true;

        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = accountType;
            SetInterestCalculationStrategy();
            transactions = new List<Transaction>();
        }

        private void SetInterestCalculationStrategy()
        {
            switch (accountType)
            {
                case (int)AccountTypes.SAVINGS:
                    interest = new InterestSavings();
                    break;
                case (int)AccountTypes.MAXI_SAVINGS:
                    interest = new InterestMaxiSavings();
                    break;
                default:
                    interest = new InterestCheckingAccounts();
                    break;
            }
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public double InterestEarned()
        {
            double amount = 0;
            if (transactions.Any())
            {
                amount = interest.CalculateInterest(SumTransactions(), noWithdrawalsInTenDays);
            }
            return amount;
        }

        /// <summary>
        /// Change Maxi-Savings accounts to have an interest rate of 5% assuming no withdrawals in the past 10 days otherwise 0.1%
        /// </summary>
        /// <returns></returns>
        public double SumTransactions()
        {
            double amount = 0;
            foreach (var transaction in transactions)
            {
                if (accountType == (int)AccountTypes.MAXI_SAVINGS && transaction.transactionDate >= DateTime.Now.AddDays(-10) && transaction.amount < 0)
                {
                    noWithdrawalsInTenDays = false;
                }
                amount += transaction.amount;
            }
            return amount;
        }

        public int GetAccountType()
        {
            return accountType;
        }

        public string GetPrettyAccountType()
        {
            string prettyAccountType = string.Empty;

            switch (accountType)
            {
                case (int)AccountTypes.CHECKING:
                    prettyAccountType = "Checking Account";
                    break;
                case (int)AccountTypes.SAVINGS:
                    prettyAccountType = "Savings Account";
                    break;
                case (int)AccountTypes.MAXI_SAVINGS:
                    prettyAccountType = "Maxi Savings Account";
                    break;
            }
            return prettyAccountType;
        }
    }
}

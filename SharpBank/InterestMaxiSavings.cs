﻿namespace SharpBank
{
    class InterestMaxiSavings : Interest
    {
        /// <summary>
        /// Change Maxi-Savings accounts to have an interest rate of 5% assuming no withdrawals in the past 10 days otherwise 0.1%
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public override double CalculateInterest(double amount, bool noWithdrawalsInTenDays)
        {
            if (noWithdrawalsInTenDays)
                return amount * 0.05;
            else
                return amount * 0.1;
        }
    }
}
